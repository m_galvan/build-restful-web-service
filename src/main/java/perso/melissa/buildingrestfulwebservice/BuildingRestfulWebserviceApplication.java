package perso.melissa.buildingrestfulwebservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildingRestfulWebserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuildingRestfulWebserviceApplication.class, args);
	}

}
