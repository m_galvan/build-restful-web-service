# Build a RESTful Web Service 

Here I tried to create a small RESTful web service with the Spring documentation.
I know it's not like a real web service but it's to develop good habits.

[Spring](https://spring.io/guides/gs/rest-service/) -> The documentation I followed.

I created 2 files :
* Greeting.java
* GreetingController.java
  
**Greeting.java** is a class with properties, constructor and methods(getters).

**GreetingController** is a class who use Greeting.java to return Json response. 